package TestCases;
import objectRepo.WebFormApp;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;


public class testFillForm {


    @Test
    public void testSummitForm(){

        System.setProperty("webdriver.chrome.driver", "/home/ralcaraz/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/form");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebFormApp webFormApp = new WebFormApp(driver);
        webFormApp.Username().sendKeys("Raul Antonio");
        webFormApp.LastName().sendKeys("Alcaraz");
        webFormApp.JobTitle().sendKeys("Automation Tester");
        webFormApp.RadioButton().click();
        webFormApp.CheckBox().click();
        webFormApp.Experience().click();
        webFormApp.DatePicker().click();
        webFormApp.TodayDate().click();
        webFormApp.Submit().click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
        assertEquals(webFormApp.Alert(), "The form was successfully submitted!");
        driver.close();
    }

}
