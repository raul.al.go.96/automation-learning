package objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebFormApp {

    WebDriver driver;

    public WebFormApp(WebDriver driver) {
        this.driver = driver;
    }
    By username = By.cssSelector("#first-name");
    By lastName = By.cssSelector("#last-name");
    By jobTitle = By.cssSelector("#job-title");
    By radioButton = By.cssSelector("#radio-button-2");
    By checkBox = By.cssSelector("#checkbox-1");
    By experience = By.cssSelector("option[value='1']");
    By datePicker = By.cssSelector("#datepicker");
    By todayDate = By.cssSelector(".today.day");
    By submit = By.cssSelector("a[role='button']");
    By alert = By.cssSelector("div[role='alert']");


    public WebElement Username()
    {
        return driver.findElement(username);

    }
    public WebElement LastName()
    {
        return driver.findElement(lastName);
    }
    public WebElement JobTitle()
    {
        return driver.findElement(jobTitle);
    }
    public WebElement RadioButton()
    {
        return driver.findElement(radioButton);
    }
    public WebElement CheckBox()
    {
        return driver.findElement(checkBox);
    }
    public WebElement Experience()
    {
        return driver.findElement(experience);
    }
    public WebElement DatePicker()
    {
        return driver.findElement(datePicker);
    }
    public WebElement TodayDate()
    {
        return driver.findElement(todayDate);
    }
    public WebElement Submit()
    {
        return driver.findElement(submit);

    }
    public String Alert()
    {
        return driver.findElement(alert).getText();
    }
}
