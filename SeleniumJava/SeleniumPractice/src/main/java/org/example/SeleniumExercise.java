package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumExercise {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "/home/ralcaraz/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/form");
        driver.findElement(By.cssSelector("#first-name")).sendKeys("Raul");
        driver.findElement(By.cssSelector("#last-name")).sendKeys("Alcaraz");
        driver.findElement(By.cssSelector("#job-title")).sendKeys("Automation Tester");
        driver.findElement(By.cssSelector("#radio-button-2")).click();
        driver.findElement(By.cssSelector("#checkbox-2")).click();
        driver.findElement(By.cssSelector("option[value='1']")).click();
        driver.findElement(By.cssSelector("#datepicker")).click();
        driver.findElement(By.cssSelector(".today.day")).click();
        driver.findElement(By.cssSelector("a[role='button']")).click();
        driver.close();
    }
}
