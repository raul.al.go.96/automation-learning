# Log4j Assignment -1

## What is the advantages of using Log4j?

Log4j Is a great logging pluguin, it beat normal java loggin because you can decouple your loggin code from what you want to log and where and how you want to log it.
Details about logging verbosity/filtering, formatting, log location, and even log type (files, network, etc.) are handled declaratively using configuration and extensibly via custom appenders, rather you having to code that flexibility yourself. 

Some of its most notable advantages are:

* Good logging infrastructure without putting in any efforts.

* Ability to categorize logs at different levels (Trace, Debug, Info, Warn, Error and Fatal).

* Direct logs to different outputs. For e.g. to a file, Console or a Database.

* Lets you define the format of output logs.

* Write Asynchronous logs which helps to increase the performance of the application.

* Loggers in Log4j follow a class hierarchy which may come handy to your applications.

## What are the different dependency jars we add before using Log4j in a java project.

To download the necesary jars to use Log4j you can visit https://logging.apache.org/log4j/2.x/download.html and download any mirror, after that just add it to your path in any IDE. Another solution is to use a project management tool like Maven, to add Log4j using maven you can add the following code in Maven:

```
<!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core -->
<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-core</artifactId>
    <version>2.17.1</version>
</dependency>
```

## What are the different levels of loggin in log4j 

The following levels are normally avaliable ofr Log4j, however you can add your ouwn custom levels as you see fit:

| Level | Description                                                                                               |
|-------|-----------------------------------------------------------------------------------------------------------|
| ALL   | All levels including custom levels.                                                                       |
| DEBUG | Designates fine-grained informational events that are most useful to debug an application.                |
| INFO  | Designates informational messages that highlight the progress of the application at coarse-grained level. |
| WARN  | Designates potentially harmful situations.                                                                |
| ERROR | Designates error events that might still allow the application to continue running.                       |
| FATAL | Designates very severe error events that will presumably lead the application to abort.                   |
| OFF   | The highest possible rank and is intended to turn off logging.                                            |
| TRACE | Designates finer-grained informational events than the DEBUG.                                             |

The following example show how we can filter all out debug and info messages: This program uses logger method setLevel(Level.x) to set a desired loggin level:

```
import org.apache.log4j.*;

public class LogClass {
   private static org.apache.log4j.Logger log = Logger.getLogger(LogClass.class);
   
   public static void main(String[] args) {
      log.setLevel(Level.WARN);

      log.trace("Trace Message!");
      log.debug("Debug Message!");
      log.info("Info Message!");
      log.warn("Warn Message!");
      log.error("Error Message!");
      log.fatal("Fatal Message!");
   }
}
```
When you compile and run the LogClass program, it would generate the following result −

```
Warn Message!
Error Message!
Fatal Message!

```

Explan 4 components of Log4j.properties file:


The log4j.Properties file looks as following:

```
name=PropertiesConfig
property.filename = logs
appenders = console, file
appender.console.type = Console
appender.console.name = STDOUT
appender.console.layout.type = PatternLayout
appender.console.layout.pattern = [%-5level] %d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %c{1} - %msg%n
appender.file.type = File
appender.file.name = LOGFILE
appender.file.fileName=${filename}/propertieslogs.log
appender.file.layout.type=PatternLayout
appender.file.layout.pattern=[%-5level] %d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %c{1} - %msg%n
loggers=file
logger.file.name=Calculator
logger.file.level = debug
logger.file.appenderRefs = file
logger.file.appenderRef.file.ref = LOGFILE
rootLogger.level = debug
rootLogger.appenderRefs = stdout
rootLogger.appenderRef.stdout.ref = STDOUT
```

1. appender.file.fileName=${filename}/propertieslogs.log: This line of code designates where and how the the logging file will be located and what will be its name.

2. logger.file.name=Calculator: In this example, this line of code represents the name of the package log4j will operate in.

3. appender.file.layout.pattern=[%-5level] %d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %c{1} - %msg%n: This line shows the format in wich messages will be displayed, for example this format will print in the following manner:

```
[INFO ] 2021-12-28 16:00:58.176 [main] Log4jDemo - This is information message
[ERROR] 2021-12-28 16:00:58.178 [main] Log4jDemo - This is error msg
[WARN ] 2021-12-28 16:00:58.178 [main] Log4jDemo - This is a warning msg
[FATAL] 2021-12-28 16:00:58.178 [main] Log4jDemo - This is fatal msg
```

4. appenders = console, file : This line of code is used to choose where you want your logs to be generated,in this example using " console, file " the output of out logger will show both in out loggin file and our console.










