package Calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class DivisionTest {
    public DivisionTest(){

    }
    @Test
    public void testDivision(){
        System.out.println("This will test the division");
        int numerator = 0;
        int expectedResult = 0;
        int denominator = 0;
        Division division =  new Division();
        int result;
        numerator = 10;
        denominator = 5;
        expectedResult = 2;
        result = division.quotient(numerator, denominator);
        assertEquals(expectedResult,result);
    }
    @Test
    public void divide_whenDenominatorIsZero_shouldThrow() {
        assertThrows(ArithmeticException.class, () -> quotient(1, 0));
        System.out.println("Arithmetic Exception detected! Denominator is 0!");
    }

    private int quotient(int a, int b) {
        return a / b;
    }

}