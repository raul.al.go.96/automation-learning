package Calculator;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;
@RunWith(value = Parameterized.class)
public class MultiplyTest {
    private int a;
    private int b;
    private int expected;

    public MultiplyTest(int a, int b, int expected) {
        this.a = a;
        this.b = b;
        this.expected = expected;
    }
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, 1, 1},
                {2, 2, 4},
                {3, 2, 6},
                {4, 5, 20},
                {5, 5, 25},
                {6, 6, 36},
                {8, 8, 64}
        });
    }
    @Test
    public void test_multiplyTwoNumbersParameters()
    {
        assertEquals(Math.multiplyExact(a,b), expected);
    }
    @Test
    public void test_multiplyTwoNumbers()
    {
        Multiply multiply = new Multiply();
        int answer = multiply.multiply(5,6);
        expected = 30;
        assertEquals(answer,expected);
    }
}