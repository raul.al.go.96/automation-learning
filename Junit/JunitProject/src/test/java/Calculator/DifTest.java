package Calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class DifTest {
    public DifTest(){

    }
    @Test
    public void testDif(){
        System.out.println("This will test the subtraction");
        int a = 17;
        int b = 5;
        Dif instance = new Dif();
        int expResult = 12;
        int result = instance.minus(a, b);
        assertEquals(expResult, result);
    }

}