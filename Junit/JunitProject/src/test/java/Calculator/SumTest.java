package Calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class SumTest {
    public SumTest(){

    }
    @Test
    public void testSummation(){
        System.out.println("This will perform a test for the Sum Class.");
        int[] nums = {1,2,3,4,5};
        Sum sum = new Sum();
        int expectedResult = 15;
        int result = sum.summation(nums);
        assertEquals(expectedResult,result);
    }

}