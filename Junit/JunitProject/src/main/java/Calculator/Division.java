package Calculator;

public class Division {
    public int quotient(int numerator, int denominator)
    {
        try {
            return numerator / denominator;
        }
        catch (ArithmeticException e) {
            throw new ArithmeticException("This is dividing by 0!");
        }
    }

}
