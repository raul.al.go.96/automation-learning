# Junit Assignment

#### What is the advantage of using Junit Test for Java:

During the process of testing the applications developers may face many problems. The flow of an application cannot be tested only using main() method. For example, for a web application if we want to test the flow we need to deploy it on the server and if there is a change in java code the application should be restarted again. Testing time will increase.

*The most important benefits of JUnit framework are:*

* Simple framework for writing automated, self-verifying tests in Java
* Support for test assertions
* Test suite development
* Immediate test reporting

#### What diferent JARS are required for Junit Execution In IDE tool:

To install the lastest release of Junit; visit the JUnit official site: http://junit.org/junit4/ here you can download the lastest version of JUnit.jar and
simply add it to your test path.
However, the best way to add JUnit to your workspace might be using a Project Management tool like meaven, using maven makes adding JARS and managing them much easier, you just need to add the following lines to you POM.xml and you are good to go:
```
<!-- https://mvnrepository.com/artifact/junit/junit -->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.13.2</version>
    <scope>test</scope>
</dependency>
```



#### What are different **types of Assertion methods** in Junit:

In this version of the library, assertions are available for all primitive types, Objects, and arrays (either of primitives or Objects).

The parameters order, within the assertion, is the expected value followed by the actual value; optionally the first parameter can be a String message that represents the message output of the evaluated condition.

For example:

1. AssertEquals:

```
@Test
public void whenAssertingEquality_thenEqual() {
    String expected = "ralcaraz";
    String actual = "ralcaraz";

    assertEquals(expected, actual);
}
```

2. assertArrayEquals

If we want to assert that two arrays are equals, we can use the assertArrayEquals:

```
@Test
public void whenAssertingArraysEquality_thenEqual() {
    char[] expected = {'J','u','n','i','t'};
    char[] actual = "Junit".toCharArray();
    
    assertArrayEquals(expected, actual);
}
```
3. assertNotNull and assertNull
When we want to test if an object is null we can use the assertNull assertion:
```
@Test
public void whenAssertingNull_thenTrue() {
    Object car = null;
    
    assertNull("The car should be null", car);
}
```
4. assertTrue and assertFalse
```
@Test
public void whenAssertingConditions_thenVerified() {
    assertTrue("5 is greater then 4", 5 > 4);
    assertFalse("5 is not greater then 6", 5 > 6);
}
```
5. fail
The fail assertion fails a test throwing an AssertionFailedError. It can be used to verify that an actual exception is thrown or when we want to make a test failing during its development.

```
@Test
public void whenCheckingExceptionMessage_thenEqual() {
    try {
        methodThatShouldThrowException();
        fail("Exception not thrown");
    } catch (UnsupportedOperationException e) {
        assertEquals("Operation Not Supported", e.getMessage());
    }
}
```

6. assertThat
The assertThat assertion is the only one in JUnit 4 that has a reverse order of the parameters compared to the other assertions.

```

@Test
public void testAssertThatHasItems() {
    assertThat(
      Arrays.asList("Java", "Kotlin", "Scala"), 
      hasItems("Java", "Kotlin"));
}
```
#### What is the purpose of Test Suite:
Test suite is used to bundle a few unit test cases and run them together. In JUnit, both @RunWith and @Suite annotations are used to run the suite tests. This example uses two test classes, TestJunit1 & TestJunit2, that run together using Test Suite.

first we can create out two classes to be tested, TestJunit1 & TestJunit2:
```
import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;

public class TestJunit1 {

   String message = "Robert";	
   MessageUtil messageUtil = new MessageUtil(message);
   
   @Test
   public void testPrintMessage() {	
      System.out.println("Inside testPrintMessage()");    
      assertEquals(message, messageUtil.printMessage());     
   }
}

public class TestJunit2 {

   String message = "Robert";	
   MessageUtil messageUtil = new MessageUtil(message);
 
   @Test
   public void testSalutationMessage() {
      System.out.println("Inside testSalutationMessage()");
      message = "Hi!" + "Robert";
      assertEquals(message,messageUtil.salutationMessage());
   }
}

```

Then we create a java class file named TestSuite.java:

``` 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   TestJunit1.class,
   TestJunit2.class
})

public class JunitTestSuite {   
}  	
```

Then, we can create a java class named TestRunner.java to execute the test cases:
```
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(JunitTestSuite.class);

      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
		
      System.out.println(result.wasSuccessful());
   }
}  	
```
In this scenario we can bundle as many tests as we want, making it easier to execute our test cases and re-using them at apropiate times.


#### How can you ignore a Junit test in Test Suite
Using the same classes that we viwed in the previus example, ignoring the test is a simple process of using the **@ignore** anotation:

```
public class TestJunit2 {

   String message = "Robert";	
   MessageUtil messageUtil = new MessageUtil(message);
   @Ignore
   @Test
   public void testSalutationMessage() {
      System.out.println("Inside testSalutationMessage()");
      message = "Hi!" + "Robert";
      assertEquals(message,messageUtil.salutationMessage());
   }
}
```

As we can see, simple adding **@Ignore** to out class will be enough to skip this test when we run our suite.


#### How can we parameterize a JunitTest or pass a parameter to Junit:

In this example we will se how to parameterize a test, Junit 4 introduces a new feature called parameterized tests, this test allow a developer to run the same
test over and over using diferent values, there are five steps required to create this test:

* Annotate test class with @RunWith(Parameterized.class).

* Create a public static method annotated with @Parameters that returns a Collection of Objects (as Array) as test data set.

* Create a public constructor that takes in what is equivalent to one "row" of test data.

* Create an instance variable for each "column" of test data.

* Create your test case(s) using the instance variables as the source of the test data.

For example, we will create a class that checks for prime numbers, lets call it **PrimeNumberChecker**:

```
public class PrimeNumberChecker {
   public Boolean validate(final Integer primeNumber) {
      for (int i = 2; i < (primeNumber / 2); i++) {
         if (primeNumber % i == 0) {
            return false;
         }
      }
      return true;
   }
}
```

Now lets create a parameterized test case class:

```
RunWith(Parameterized.class)
public class PrimeNumberCheckerTest {
   private Integer inputNumber;
   private Boolean expectedResult;
   private PrimeNumberChecker primeNumberChecker;

   @Before
   public void initialize() {
      primeNumberChecker = new PrimeNumberChecker();
   }
	
   public PrimeNumberCheckerTest(Integer inputNumber, Boolean expectedResult) {
      this.inputNumber = inputNumber;
      this.expectedResult = expectedResult;
   }

   @Parameterized.Parameters
   public static Collection primeNumbers() {
      return Arrays.asList(new Object[][] {
         { 2, true },
         { 6, false },
         { 19, true },
         { 22, false },
         { 23, true }
      });
   }

   // This test will run 4 times since we have 5 parameters defined
   @Test
   public void testPrimeNumberChecker() {
      System.out.println("Parameterized Number is : " + inputNumber);
      assertEquals(expectedResult, 
      primeNumberChecker.validate(inputNumber));
   }
}
```
In this example we are running 5 parameters that we have declared in out Collection, this is an Array of numbers and array of answers true or false, depending if the number is a prime number or not. Parameterization a great tool to try to find errors and save time.











