# Automation Learning

Automation Learning

## Absolute Xpath
An absolute path is a path that describes the location of a file or folder regardless of the current
working directory; in fact, it is relative to the root directory. It contains the complete location of
a file or directory, hence the name. It is also referred to as absolute pathname or full path and it
always starts at the same place, which is the root directory. Absolute paths contain all the
relevant information to find the resources indicated by an absolute URL. An absolute path must
be used to refer to websites that are on a domain other than your home domain.


## Relative Xpath

A relative path is a path that describes the location of a file or folder in relative to the current
working directory. It can be best used to refer to websites that are located on the same
domain, ideally on certain sections of websites in which the documents never change
relationships to each other. Unlike absolute paths, relative paths contain information that is
only relative to the current document within the same website which avoids the need to
provide a full absolute path. In simple words, relative path refers to a path relative to the
location of the current webpage.


## Contains
Contains() is a method which is used to find the value of those attribute which changes
dynamically. For example, login information.
Contains() method has the following general form which is given below:

```

Xpath : //tagname[contains(@attribute, 'value')]

```

## Start-with
Starts-with() is a method that finds those elements whose attribute value changes on refresh
on the web page. This method checks the starting text of an attribute and finds elements
whose attribute changes dynamically.
We can also use this method to find elements whose attribute value is static (not changing).
The general syntax for starts-with() method is given below:

```
Xpath: //tagname[starts-with(@attribute, ' value ' )]

```

## Following-sibling

The following-sibling selects all sibling nodes after the current node at the same level. i.e. It will
find the element after the current node.

```
Xpath(radio button) // input[@id = 'value']

```

## Preceding-sibling
The preceding-sibling axis selects all siblings before the current node. Let’s take an example to
understand the concept of the preceding-sibling axis.
Open web page www.pixabay.com, right-click on videos link and go to inspect option.
Let’s consider videos link as current node as shown in below screenshot and find the XPath of
current node by using text() method

```
Xpath(current node) : //a[text() = 'Video']

```
The above expression identified three nodes before the current node (videos link) as shown in
above screenshot.
Using this expression, we can easily find XPath of preceding-sibling elements like Photos,
Illustrations, and Vectors like this:


```
Xpath(photos) : //a[text() = 'Videos]// preceding-sibling :: a[3]

```

## Css selector
CSS selectors are used to "find" (or select) the HTML elements you want to style.
We can divide CSS selectors into five categories:
Simple selectors (select elements based on name, id, class)
Combinator selectors (select elements based on a specific relationship between them)
Pseudo-class selectors (select elements based on a certain state)
Pseudo-elements selectors (select and style a part of an element)
Attribute selectors (select elements based on an attribute or attribute value)

