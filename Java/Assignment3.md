# Automation Learning

# Assignment 3:

## 1. How does java implement encapsulation?

Encapsulation in java is achieved by declaring the variables of a cass as private and providing public setters and getters methods to modify and view the
variables values.

For example, we can declare a class "Student" wich has a private variable name. Then we create getter and setter metods to get and set the name of a student
with the help of these methods, any class that wants to access the name has to do so using the getter and setter methods.

```
public class Student {
private String name;
public String getName() {       
return name;        
}       
public void setName(String name) {      
this.name = name;       
}       
}       
class Test{     
public static void main(String[] args) {        
Student s=new Student();        
s.setName("Harry Potter");      
System.out.println(s.getName());        
}       
}
```
Encapsulation is essential in java because it **controls the way in wich data can be accessed**, it also modifies the code based on the requisites and help us achieve a loose couple. This then achieves simplicity of our aplication and allows us to change part of out code without disrupting any other funtions or code.


## How does Java implement Polymorphism

Polymorphism in java is the ability of an object to take many formsSpeed Limit is: 150. It allows us to perform the same action in many different ways. Any java object that can pass mora than one "IS - A" is considered to be a polymorphic object. In a technicald world, polymorphism in java allows one to do multiple implementations by **defining one interface**.

For example, a super class named "Shapes" has a method "area()". Subclasess of this class can be "Triangle", "Circle", "Recangle", etc. Each subclass has its own way of calculating area. Using inheritance and polymorphism, the subclasses can use the area() method to find the area for the fiven shape.

```	
class Shapes {
  public void area() {
    System.out.println("The formula for area of ");
  }
}
class Triangle extends Shapes {
  public void area() {
    System.out.println("Triangle is ½ * base * height ");
  }
}
class Circle extends Shapes {
  public void area() {
    System.out.println("Math.PI * radius * radius ");
  }
}
class Main {
  public static void main(String[] args) {
    Shapes myShape = new Shapes();  // Create a Shapes object
    Shapes myTriangle = new Triangle();  // Create a Triangle object
    Shapes myCircle = new Circle();  // Create a Circle object
    myShape.area();
    myTriangle.area();
    myShape.area();
    myCircle.area();
  }
}
```

## What is the diference between overloading and overriding

The first diference between this two is when they ocurr, **Overloading happens at compile-time while Overriding happens at runtime**: The binding of overloaded method call to its definition has happens at compile-time however binding of overridden method call to its definition happens at runtime.
Another important diference is which methods can be overloaded, Static methods can be overloaded which means a class can have more than one static method of same name. Static methods **cannot be overridden**, even if you declare a same static method in child class it has nothing to do with the same method of parent class.
We can see this basically as that overloading is being done in the same class while overriding requires child and base classes.

For example:
### Overloading
```
class Sum
{
    int add(int n1, int n2) 
    {
        return n1+n2;
    }
    int add(int n1, int n2, int n3) 
    {
        return n1+n2+n3;
    }
    int add(int n1, int n2, int n3, int n4) 
    {
        return n1+n2+n3+n4;
    }
    int add(int n1, int n2, int n3, int n4, int n5) 
    {
        return n1+n2+n3+n4+n5;
    }
    public static void main(String args[])
    {
    	Sum obj = new Sum();
    	System.out.println("Sum of two numbers: "+obj.add(20, 21));
    	System.out.println("Sum of three numbers: "+obj.add(20, 21, 22));
    	System.out.println("Sum of four numbers: "+obj.add(20, 21, 22, 23));
    	System.out.println("Sum of five numbers: "+obj.add(20, 21, 22, 23, 24));
    }
```
The output of this program would be:

```
Sum of two numbers: 41
Sum of three numbers: 63
Sum of four numbers: 86
Sum of five numbers: 110
```
In this example, we can see how we are Overloading the method by adding **more versions of the same methods**,  in this case, the add() method.

### Overriding

```
package beginnersbook.com;
class CarClass
{
    public int speedLimit() 
    {
        return 100;
    }
}
class Ford extends CarClass
{
    public int speedLimit()
    {
        return 150;
    }
    public static void main(String args[])
    {
    	CarClass obj = new Ford();
    	int num= obj.speedLimit();
    	System.out.println("Speed Limit is: "+num);
    }
}
```
The output would then be:
```
Speed Limit is: 150
```

In this case, speedLimit() method of class Ford is overriding the speedLimit() method of CarClass.

## Diferences between abstract and inheritance.

A class which has the abstract keyword in its declaration is called abstract class. Abstract classes should have at least one abstract method. i.r, methods without a body. it is important to notice that **abstract classes cannot be instantiated**.

Important reasions for using abstract classes.
1. Interfaces are used to achieve abstraction.
2. Designed to support dynamic method resolution at run time
3. It helps you to achieve loose coupling.
4. Allows you to separate the definition of a method from the inheritance hierarchy

Important reasons for using abstract classes:

1. Abstract classes offer default functionality for the subclasses.
2. Provides a template for future specific classes
3. Helps you to define a common interface for its subclasses
4. Abstract class allows code reusability.

An example of a java interface can be:

```
interface Pet {
    public void test();
}
class Dog implements Pet {
    public void test() {
        System.out.println("Interface Method Implemented");
    }
    public static void main(String args[]) {
        Pet p = new Dog();
        p.test();
    }
}
```

And an abstract class can be:

```
abstract class Shape {
    int b = 20;
    abstract public void calculateArea();
}

public class Rectangle extends Shape {
    public static void main(String args[]) {
        Rectangle obj = new Rectangle();
        obj.b = 200;
        obj.calculateArea();
    }
    public void calculateArea() {
        System.out.println("Area is " + (b * b));
    }
}
```

## How can we compare 2 objects of the same class in java.

By implementing a compareTo, we can provide an implementation that returns a positive value if the object comes after the other,
zero if both are equal or a negative calue if the calling of the object comes before.

For example:

```
    @Override
    public int compareTo(Object a) {
        //this compareTo method allows the programmer to sort addresses by zip 
        //code
        Address addr = (Address)a;
        int zip1 = Integer.parseInt(this.zipcode);
        int zip2 = Integer.parseInt(addr.zipcode);
        if(zip1 < zip2)
            return -1;
        else if (zip1 > zip2)
            return 1;
        else
            return 0;
    }
```
In this case, we are comparing an atribute of a class named adress, first we need to **@overrride**, and use the compareTo to compare both objects,
then cast the object, in this case as an adress, and compare an object from this. Then we can implement an IF condition to return positive, negative or 0 values.

































