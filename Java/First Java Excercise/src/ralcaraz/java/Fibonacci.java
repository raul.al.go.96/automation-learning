package ralcaraz.java;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.println("Displaying the first 5 Fibonacci numbers");
        printFibonacci(5);
        System.out.println("");
        System.out.println("Displaying the first 8 Fibonacci numbers");
        printFibonacci(8);
    }
    static void printFibonacci(int N)
    {
        int num1 = 0;
        int num2 = 1;
        int i = 0;
        while(i< N)
        {
            System.out.print(num1 + ", ");
            int num3 = num2 + num1;
            num1 = num2;
            num2 = num3;
            i++;
        }
    }
}
