package ralcaraz.java;

public class GreaterNumberOverload {
    public static void main(String[] args) {
        System.out.println("The biggest number is " + findBiggerInt(4, 10, 6));

    }

    static int findBiggerInt(int firstNumber, int secondNumber, int thirdNumber) {
        int result = 0;
        if (firstNumber >= secondNumber && firstNumber >= thirdNumber) {
            result = firstNumber;
        } else if (secondNumber >= firstNumber && secondNumber >= thirdNumber) {
            result = secondNumber;
        } else if (thirdNumber >= firstNumber && thirdNumber >= secondNumber) {
            result = thirdNumber;
        }
        return result;
    }

