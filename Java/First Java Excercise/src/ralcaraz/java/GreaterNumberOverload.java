package ralcaraz.java;

public class GreaterNumberOverload {
    public static void main(String[] args) {
        System.out.println("The biggest number is " + findBiggerInt(4, 10, 6));
        System.out.println("The biggest number is " + findBiggerInt(2, 1, 4, 5));

    }

    static int findBiggerInt(int firstNumber, int secondNumber, int thirdNumber) {
        int result = 0;
        if (firstNumber >= secondNumber && firstNumber >= thirdNumber) {
            result = firstNumber;
        } else if (secondNumber >= firstNumber && secondNumber >= thirdNumber) {
            result = secondNumber;
        } else if (thirdNumber >= firstNumber && thirdNumber >= secondNumber) {
            result = thirdNumber;
        }
        return result;
    }

    static int findBiggerInt(int firstNumber, int secondNumber, int thirdNumber, int fourthNumber) {
        int result = 0;
        if (firstNumber >= secondNumber && firstNumber >= thirdNumber && firstNumber >= fourthNumber) {
            result = firstNumber;
        } else if (secondNumber >= firstNumber && secondNumber >= thirdNumber && secondNumber >= fourthNumber) {
            result = secondNumber;
        } else if (thirdNumber >= firstNumber && thirdNumber >= secondNumber && thirdNumber >= fourthNumber) {
            result = thirdNumber;
        } else if (fourthNumber >= firstNumber && fourthNumber >= secondNumber && fourthNumber >= thirdNumber) {
            result = fourthNumber;
        }
        return result;
    }
}


