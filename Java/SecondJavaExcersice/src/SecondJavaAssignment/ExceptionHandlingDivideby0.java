package SecondJavaAssignment;

import java.util.Scanner;

public class ExceptionHandlingDivideby0 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("This program will divide two numbers");
        System.out.println("Please type your first number");
        int firstNum = scanner.nextInt();
        System.out.println("Type your seccond number");
        int secondNum = scanner.nextInt();

        try {
            System.out.println(firstNum / secondNum);
        }
        catch (ArithmeticException e)
        {
            System.out.println("One of your numbers is 0, this division is not possible.");
        }

    }

}
