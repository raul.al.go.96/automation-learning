package SecondJavaAssignment;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;

public class Student {
    public Student(String firstName, String middleName, String lastName, String DOB,int studentID ){
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.DOB = DOB;
    }


    public String getName(){
        return firstName + " " +  middleName + " " + lastName;
    }
    public int getStudentID() {
        Random rand = new Random();
        this.studentID = studentID * rand.nextInt((10 - 2) + 1) +1;
        return studentID;
    }

    public void setStudentID(int value) {
        this.studentID = value;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String value) {
        this.middleName = value;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public String getDOB() throws ParseException {
        String date = DOB;
        String[] dates = date.split("-");
        String reformattedDate = String.format("%s-%s-%s", dates[2], dates[1], dates[0]);
        this.DOB = reformattedDate;
        return DOB;
    }

    public void setDOB(String value) {

        this.DOB = value;
    }
    private int studentID;
    private String firstName;
    private String middleName;
    private String lastName;
    private String DOB;


    public static void main(String[] args) throws ParseException {
        Student s1 = new Student("Raul", "Antonio", "Alcaraz", "15-02-2021", 1);
        Student s2 = new Student("Juan", "Fernando", "Gomez", "30-04-2020", 1);
        Student s3 = new Student("Jose", "Roberto", "Lopez", "22-05-2019", 1);

        System.out.println(s1.getName());
        System.out.println(s2.getName());
        System.out.println(s3.getName());
        System.out.println(s1.getDOB());
        System.out.println(s2.getDOB());
        System.out.println(s3.getDOB());
        System.out.println("student id " + s1.getStudentID());
    }

}
