package SecondJavaAssignment;

public class StudentInstance {
    public static void main(String[] args) {
        Student s1 = new Student("Raul", "Antonio", "Alcaraz", "15/02/2021",1);
        Student s2 = new Student("Juan", "Fernando", "Gomez", "30/04/2020",2);
        Student s3 = new Student("Jose", "Roberto", "Lopez", "22/05/2019", 3);

        System.out.println(s1.getName());
        System.out.println(s2.getName());
        System.out.println(s3.getName());
    }
}
