package SecondJavaAssignment;

import java.util.Scanner;

public class ExceptionHandlingEmptyString {
    public static void main(String[] args) {
        String firstString = "abc";
        String secondString = null;
        if(firstString == null || secondString == null )
        {
            throw new NullPointerException("One of your strings is empty");
        }
    }
}
