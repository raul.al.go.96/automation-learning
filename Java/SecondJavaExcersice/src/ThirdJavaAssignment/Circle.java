package ThirdJavaAssignment;

import java.util.Scanner;

public class Circle extends Shape {
    Scanner scanner = new Scanner(System.in);
    public Circle()
    {
        super("Circle");
    }
    public Circle (String name)
    {
        super(name);
    }
    public double calculateArea()
    {
        System.out.println("Enter the radius of the circle: ");
        double radius = scanner.nextDouble();
        return Math.PI * Math.pow(radius, 2);

    }
}
