package ThirdJavaAssignment;

import java.util.Scanner;

public class Rectangle extends Shape{
    Scanner scanner = new Scanner(System.in);
    public Rectangle(){
        super("Rectangle");
    }
    public Rectangle (String name)
    {
        super(name);
    }
    public double calculateArea()
    {
        System.out.println("Enter height");
        double height = scanner.nextDouble();
        System.out.println("Enter base");
        double base = scanner.nextDouble();

        return (base * height / 2);
    }
}
